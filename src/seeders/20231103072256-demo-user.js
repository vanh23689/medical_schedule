'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    return queryInterface.bulkInsert('Users', [{
      firstName: 'Test',
      lastName: 'Case',
      email: 'testcase@gmail.com',
      password: '123456',
      phoneNumber: '012345678',
      address: 'VN',
      gender: 1,
      roleId: 2,
      createdAt: new Date(),
      updatedAt: new Date()
    }])
  },

  async down(queryInterface, Sequelize) {
    return queryInterface.bulkDelete('Users', null, {});
  }
};
