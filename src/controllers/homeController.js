import db from '../models/index';
import todoListService from '../services/todoListService';

let getHomePage = async (req, res) => {
    try {
        let data = todoListService.getAllUser();
        return res.render('homePage.ejs', {
            data: JSON.stringify(data)
        });
    } catch (error) {
    }
}
let getAboutPage = (req, res) => {
    return res.render('test/about.ejs');
}

let allToDoList = async (req, res) => {
    let data = await todoListService.getAllUser();
    return res.render('todoList.ejs', {
        data: data
    });
}

let postToDoList = async (req, res) => {
    let message = await todoListService.createNewUser(req.body)
    return res.send('Post todolist create')
}

let todoList = async (req, res) => {
    let data = await todoListService.getAllUser();

    console.log("Get all users");
}

let deleteToDoList = async (req, res) => {
    let userId = req.params.id
    let data = await todoListService.deleteUser(userId);
    if (data) {
        res.redirect('back');
    }
}

let getToDoList = async (req, res) => {
    let userId = req.params.id
    if (userId) {
        let userData = await todoListService.getUser(userId)

        return res.render('editTodoList.ejs', {
            user: userData
        })
    } else {
        return res.send("Edit fail")
    }
}

let updateToDoList = async (req, res) => {
    let userId = req.body.user_id
    if (userId) {
        let userData = await todoListService.updateUser(userId, req.body)
        return res.send("Update done")
    } else {
        return res.send("Update fail")
    }
}

module.exports = {
    getHomePage: getHomePage,
    getAboutPage: getAboutPage,
    allToDoList: allToDoList,
    postToDoList: postToDoList,
    todoList: todoList,
    getToDoList: getToDoList,
    deleteToDoList: deleteToDoList,
    updateToDoList: updateToDoList
}