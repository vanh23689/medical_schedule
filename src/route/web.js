import express from "express";
import homeController from "../controllers/homeController";
import userController from "../controllers/userController";

let router = express.Router();

let initWebRoutes = (app) => {
    router.get('/', homeController.getHomePage);
    router.get('/about', homeController.getAboutPage);
    router.get('/todolist', homeController.allToDoList);
    router.post('/todolist-store', homeController.postToDoList);
    router.post('/todolist-update', homeController.updateToDoList);
    router.get('/todolist-get/(:id)', homeController.getToDoList);
    router.get('/todolist-delete/(:id)', homeController.deleteToDoList);

    router.post('/api/login', userController.handleLogin);
    return app.use("/", router);
}

module.exports = initWebRoutes;