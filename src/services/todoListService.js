
import bcrypt from 'bcrypt';
import db from '../models/index';
const saltRounds = 10;
const salt = bcrypt.genSaltSync(saltRounds);

let createNewUser = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            let hashPasswordFromBcrypt = await hashPassword(data.password);
            await db.User.create({
                email: data.email,
                firstName: data.firstName,
                lastName: data.lastName,
                password: hashPasswordFromBcrypt,
                phoneNumber: data.phoneNumber,
                address: data.address,
                gender: data.gender === '1' ? true : false,
                roleId: data.roleId,
                positionId: data.positionId,
                image: data.image,
            })
            resolve('Create successfully !');
        } catch (error) {
            reject(error);
        }
    })
}
let hashPassword = (password) => {
    return new Promise(async (resolve, reject) => {
        try {
            let hashPassword = await bcrypt.hashSync(password, salt);
            resolve(hashPassword)
        } catch (error) {
            reject(error);
        }
    })
}

let getAllUser = async (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            let users = await db.User.findAll({
                raw: true
            })
            resolve(users)
        } catch (error) {
            reject(error);
        }
    })
}

let getUser = async (userId) => {
    return new Promise(async (resolve, reject) => {
        try {

            let user = await db.User.findOne({
                where: { id: userId },
                raw: true
            })

            if (user) {
                resolve(user)
            } else {
                resolve([])
            }
        } catch (error) {
            reject(error);
        }
    })
}

let updateUser = async (userId, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            let hashPasswordFromBcrypt = await hashPassword(data.password);
            let user = await db.User.update({
                email: data.email,
                firstName: data.firstName,
                lastName: data.lastName,
                password: hashPasswordFromBcrypt,
                phoneNumber: data.phoneNumber,
                address: data.address,
                gender: data.gender === '1' ? true : false,
                roleId: data.roleId ? data.roleId : null,
                positionId: data.positionId ? data.positionId : null,
                image: data.image ? data.image : null,
            }, {
                where: {
                    id: userId
                }
            })

            if (user) {
                resolve(user)
            } else {
                resolve([])
            }
        } catch (error) {
            reject(error);
        }
    })
}

let deleteUser = async (userId) => {
    return new Promise(async (resolve, reject) => {
        try {
            await db.User.destroy({
                where: { id: userId },
                raw: true
            })
            resolve('true');
        } catch (error) {
            reject(error);
        }
    })
}

module.exports = {
    createNewUser: createNewUser,
    getAllUser: getAllUser,
    getUser: getUser,
    updateUser: updateUser,
    deleteUser: deleteUser
}